use std::io;


fn main() {
    println!("Welcome to Simple CLI Calc! Please enter an operator (+, -, *, /) and two numbers:");

    let mut input = String::new();
    io::stdin().read_line(&mut input).expect("Failed to read line");

    let mut tokens = input.split_whitespace();
    let operator = tokens.next().unwrap();
    let x: f64 = match tokens.next() {
        Some(s) => s.parse().expect("Failed to parse number"),
        None => panic!("No value provided for x"),
    };

    let y: f64 = match tokens.next() {
        Some(s) => s.parse().expect("Failed to parse number"),
        None => panic!("No value provided for y"),
    };

    let result = match operator {
        "+" => x + y,
        "-" => x - y,
        "*" => x * y,
        "/" => x / y,
         _   => panic!("Invalid operator"),
    };

    println!("Result: {}", result);
}
# Simple CLI Calculator


This simple calculator contains no ads, nonfree code, access to your entire system, or useless features. It is just a CLI calculator written in Rust.


# How to Compile on Unix-like Systems


1. Clone the repo using `git`.

`git clone https://codeberg.org/deadlyremote/simple-cli-calc`

`cd simple-cli-calc`

2. Build with Cargo. If you encounter any errors during the compilation, open an issue.

`cargo build`

You have now installed the Simple CLI Calc! In order to run the program, run `cargo run` in the simple-cli-calc directory.